// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyB0Z2rfRHs3RMNhwuHtiKynVIy8u6o61xQ',
    authDomain: 'business-management-92547.firebaseapp.com',
    projectId: 'business-management-92547',
    storageBucket: 'business-management-92547.appspot.com',
    messagingSenderId: '63441608781',
    appId: '1:63441608781:web:b56ce5ba9e2d61e292fc04',
    measurementId: 'G-CMBQY2F582',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
