import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  // list all customers
  {
    path: 'customer',
    loadChildren: () =>
      import('./pages/customer/customer.module').then(
        (m) => m.CustomerPageModule
      ),
  },
  // add new customer
  {
    path: 'add-customer',
    loadChildren: () =>
      import('./pages/customer/add-customer/add-customer.module').then(
        (m) => m.AddCustomerPageModule
      ),
  },
  // update customer
  {
    path: 'edit-customer/:customerUuid',
    loadChildren: () =>
      import('./pages/customer/add-customer/add-customer.module').then(
        (m) => m.AddCustomerPageModule
      ),
  },
  // view single customer
  {
    path: 'customer/:customerUuid',
    loadChildren: () =>
      import('./pages/customer/view/view.module').then((m) => m.ViewPageModule),
  },
  // create new order
  {
    path: 'customer/:customerUuid/orders/add',
    loadChildren: () =>
      import('./pages/order/create-order/create-order.module').then(
        (m) => m.CreateOrderPageModule
      ),
  },
  // update order
  {
    path: 'customer/:customerUuid/orders/:orderUuid',
    loadChildren: () =>
      import('./pages/order/create-order/create-order.module').then(
        (m) => m.CreateOrderPageModule
      ),
  },
  // list all orders
  {
    path: 'customer/:customerUuid/orders',
    loadChildren: () =>
      import('./pages/order/list-order/list-order.module').then(
        (m) => m.ListOrderPageModule
      ),
  },
  // view single order
  {
    path: 'customer/:customerUuid/orders/view/:orderUuid',
    loadChildren: () =>
      import('./pages/order/view-order/view-order.module').then(
        (m) => m.ViewOrderPageModule
      ),
  },
  {
    path: 'customer/:customerUuid/add-measurement',
    loadChildren: () =>
      import('./pages/measurement/add-measurement/add-measurement.module').then(
        (m) => m.AddMeasurementPageModule
      ),
  },
  {
    path: 'customer/:customerUuid/list-measurement',
    loadChildren: () =>
      import(
        './pages/measurement/list-measurement/list-measurement.module'
      ).then((m) => m.ListMeasurementPageModule),
  },
  {
    path: 'settings',
    loadChildren: () =>
      import('./pages/settings/settings.module').then(
        (m) => m.SettingsPageModule
      ),
  },
  {
    path: 'list-all-orders',
    loadChildren: () => import('./pages/order/list-all-orders/list-all-orders.module').then( m => m.ListAllOrdersPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
