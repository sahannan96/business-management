import { Component, OnInit } from '@angular/core';
import {
  addDoc,
  collection,
  doc,
  getFirestore,
  onSnapshot,
  setDoc,
} from 'firebase/firestore';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CUSTOMER } from '../../../shared/constants/strings';
import { Customer } from '../../../shared/interfaces/list-data-interface';
import { ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.page.html',
  styleUrls: ['./add-customer.page.scss'],
})
export class AddCustomerPage implements OnInit {
  db = getFirestore();
  spinner: boolean = false;
  customer: Customer = {};
  customerForm = new FormGroup({
    name: new FormControl('', Validators.required),
    phone: new FormControl(null, [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
      Validators.minLength(10),
      Validators.maxLength(10),
    ]),
    email: new FormControl('', Validators.email),
    address: new FormControl(''),
  });

  get f() {
    return this.customerForm.controls;
  }

  constructor(
    private readonly toastController: ToastController,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.spinner = true;
    this.customer.uuid = this.route.snapshot.params.customerUuid;
    this.customer.uuid ? this.loadCustomer() : (this.spinner = false);
  }

  addCustomer() {
    this.spinner = true;
    const customer = {} as Customer;
    customer.name = this.f.name.value;
    customer.phone = parseInt(this.f.phone.value);
    customer.email = this.f.email.value;
    customer.address = this.f.address.value;

    const docRef = collection(this.db, CUSTOMER);
    addDoc(docRef, customer)
      .then(() => {
        this.spinner = false;
        this.presentToast('Customer Added');
        this.customerForm.reset();
        this.router.navigate(['/customer']);
      })
      .catch((error) => {
        this.spinner = false;
        this.presentToast(error);
      });
  }

  loadCustomer() {
    const docRef = doc(this.db, CUSTOMER, this.customer.uuid);
    onSnapshot(docRef, (doc) => {
      this.customer = {};
      this.customer = doc.data();
      this.customer.uuid = doc.id;
      this.populateForm();
      this.spinner = false;
    });
  }

  populateForm() {
    this.f.name.setValue(this.customer.name);
    this.f.phone.setValue(this.customer.phone);
    this.f.email.setValue(this.customer.email);
    this.f.address.setValue(this.customer.address);
    this.spinner = false;
  }

  updateCustomer() {
    this.spinner = true;
    const customer = {} as Customer;
    customer.name = this.f.name.value;
    customer.phone = parseInt(this.f.phone.value);
    customer.email = this.f.email.value;
    customer.address = this.f.address.value;

    const docRef = doc(this.db, CUSTOMER, this.customer.uuid);
    setDoc(docRef, customer, { merge: true })
      .then(() => {
        this.spinner = false;
        this.router.navigate(['/customer']);
        this.presentToast('Customer Updated');
      })
      .catch((error) => {
        this.spinner = false;
        this.presentToast(error);
      });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
