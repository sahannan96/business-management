import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import {
  deleteDoc,
  doc,
  getFirestore,
  onSnapshot,
  Unsubscribe,
} from 'firebase/firestore';
import { CUSTOMER } from '../../../shared/constants/strings';
import { Customer } from '../../../shared/interfaces/list-data-interface';

@Component({
  selector: 'app-view',
  templateUrl: './view.page.html',
  styleUrls: ['./view.page.scss'],
})
export class ViewPage implements OnInit {
  db = getFirestore();
  customer: Customer = {};
  unsubscribe: Unsubscribe;
  spinner: boolean = true;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly alertController: AlertController,
    private readonly toastController: ToastController
  ) {}

  ngOnInit() {
    this.customer.uuid = this.route.snapshot.params.customerUuid;
    this.loadCustomer();
  }

  loadCustomer() {
    const docRef = doc(this.db, CUSTOMER, this.customer.uuid);
    this.unsubscribe = onSnapshot(docRef, (doc) => {
      this.customer = {};
      this.customer = doc.data();
      this.customer.uuid = doc.id;
      this.spinner = false;
    });
  }

  confirmDeleteCustomer() {
    this.presentAlert(
      `Are you sure you want to delete customer ${this.customer.name}?`
    );
  }

  async presentAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      cssClass: 'custom-alert',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'alert-button-cancel',
        },
        {
          text: 'Confirm',
          role: 'confirm',
          cssClass: 'alert-button-confirm',
          handler: () => {
            this.spinner = true;
            this.deleteCustomer();
          },
        },
      ],
    });
    await alert.present();
  }

  deleteCustomer() {
    this.unsubscribe();
    const docRef = doc(this.db, CUSTOMER, this.customer.uuid);
    deleteDoc(docRef)
      .then(() => {
        this.router.navigate(['/customer']);
        this.spinner = false;
        this.presentToast('Customer Deleted');
      })
      .catch((err) => {
        this.spinner = false;
        this.presentToast(err);
      });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
