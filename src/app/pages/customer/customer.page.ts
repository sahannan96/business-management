import { Component, OnInit } from '@angular/core';
import {
  getFirestore,
  collection,
  query,
  orderBy,
  onSnapshot,
} from 'firebase/firestore';
import { CUSTOMER } from '../../shared/constants/strings';
import { Customer } from '../../shared/interfaces/list-data-interface';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.page.html',
  styleUrls: ['./customer.page.scss'],
})
export class CustomerPage implements OnInit {
  db = getFirestore();
  spinner: boolean = true;
  customers: Customer[];
  filteredList: Customer[];
  constructor() {}

  ngOnInit() {
    this.loadCustomers();
  }

  loadCustomers() {
    const docRef = collection(this.db, CUSTOMER);
    const order = query(docRef, orderBy('name', 'asc'));
    onSnapshot(order, (snapshot) => {
      this.customers = [];
      snapshot.docs.forEach((doc) => {
        let customer = {} as Customer;
        customer = doc.data();
        customer.uuid = doc.id;
        this.customers.push(customer);
      });
      this.filteredList = this.customers;
      this.spinner = false;
    });
  }

  onSearchChange(event) {
    if (event.detail.value) {
      this.filteredList = this.customers.filter((item) => {
        return (
          new RegExp(event.detail.value, 'gi').test(item['name']) ||
          event.detail.value == ''
        );
      });
    } else {
      this.filteredList = this.customers;
    }
  }
}
