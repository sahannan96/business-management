import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

import {
  getAuth,
  signInWithPopup,
  GoogleAuthProvider,
  onAuthStateChanged,
} from 'firebase/auth';
import { User } from '../../shared/interfaces/list-data-interface';
import { StorageService } from '../../shared/services/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  provider = new GoogleAuthProvider();
  auth = getAuth();
  user: User = {};
  spinner: boolean = true;
  constructor(
    private readonly storage: StorageService,
    private readonly router: Router,
    private readonly toastController: ToastController
  ) {}

  ngOnInit() {
    this.checkLogin();
  }

  checkLogin() {
    onAuthStateChanged(this.auth, (user) => {
      if (user) {
        this.storage.setItem('user', user.uid).then(() => {
          this.router.navigate(['/home']);
          this.spinner = false;
        });
      } else {
        this.spinner = false;
      }
    });
  }

  login() {
    this.spinner = true;
    signInWithPopup(this.auth, this.provider)
      .then(() => {
        this.presentToast('Successfully logged out');
        this.router.navigate(['/home']);
      })
      .catch((error) => {
        this.presentToast(error);
      });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
