import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { doc, getFirestore, onSnapshot } from 'firebase/firestore';
import { CUSTOMER } from '../../../shared/constants/strings';
import { Measurement } from '../../../shared/interfaces/list-data-interface';

@Component({
  selector: 'app-list-measurement',
  templateUrl: './list-measurement.page.html',
  styleUrls: ['./list-measurement.page.scss'],
})
export class ListMeasurementPage implements OnInit {
  db = getFirestore();
  spinner: boolean = true;
  customerUuid: string = '';
  measurement: Measurement = {};
  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit() {
    this.customerUuid = this.route.snapshot.params.customerUuid;
    this.loadMeasurements();
  }

  loadMeasurements() {
    const docRef = doc(this.db, CUSTOMER, this.customerUuid);
    onSnapshot(docRef, (doc) => {
      this.measurement = {};
      this.measurement = doc.get('measurement');
      this.spinner = false;
    });
  }
}
