import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListMeasurementPage } from './list-measurement.page';

const routes: Routes = [
  {
    path: '',
    component: ListMeasurementPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListMeasurementPageRoutingModule {}
