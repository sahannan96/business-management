import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { doc, getFirestore, onSnapshot, updateDoc } from 'firebase/firestore';
import { CUSTOMER } from '../../../shared/constants/strings';
import { Measurement } from '../../../shared/interfaces/list-data-interface';

@Component({
  selector: 'app-add-measurement',
  templateUrl: './add-measurement.page.html',
  styleUrls: ['./add-measurement.page.scss'],
})
export class AddMeasurementPage implements OnInit {
  db = getFirestore();
  spinner: boolean = false;
  measurement: Measurement = {};
  customerUuid: string = '';
  measurementForm = new FormGroup({
    top_total_length: new FormControl(),
    shoulder: new FormControl(),
    chest: new FormControl(),
    waist: new FormControl(),
    hip: new FormControl(),
    hem: new FormControl(),
    sleeve_length: new FormControl(),
    sleeve_circ: new FormControl(),
    neck_front: new FormControl(),
    neck_back: new FormControl(),

    bottom_total_length: new FormControl(),
    crotch: new FormControl(),
    thigh: new FormControl(),
    knee: new FormControl(),
    ankle: new FormControl(),
  });

  get f() {
    return this.measurementForm.controls;
  }
  constructor(
    private readonly toastController: ToastController,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.spinner = true;
    this.customerUuid = this.route.snapshot.params.customerUuid;
    this.loadMeasurements();
  }

  loadMeasurements() {
    const docRef = doc(this.db, CUSTOMER, this.customerUuid);
    onSnapshot(docRef, (doc) => {
      this.measurement = {} as Measurement;
      this.measurement = doc.get('measurement');
      this.measurement ? this.populateForm() : (this.spinner = false);
    });
  }

  populateForm() {
    this.f.top_total_length.setValue(this.measurement.top_total_length);
    this.f.shoulder.setValue(this.measurement.shoulder);
    this.f.chest.setValue(this.measurement.chest);
    this.f.waist.setValue(this.measurement.waist);
    this.f.hip.setValue(this.measurement.hip);
    this.f.hem.setValue(this.measurement.hem);
    this.f.sleeve_length.setValue(this.measurement.sleeve_length);
    this.f.sleeve_circ.setValue(this.measurement.sleeve_circ);
    this.f.neck_front.setValue(this.measurement.neck_front);
    this.f.neck_back.setValue(this.measurement.neck_back);

    this.f.bottom_total_length.setValue(this.measurement.bottom_total_length);
    this.f.crotch.setValue(this.measurement.crotch);
    this.f.thigh.setValue(this.measurement.thigh);
    this.f.knee.setValue(this.measurement.knee);
    this.f.ankle.setValue(this.measurement.ankle);

    this.spinner = false;
  }

  updateOrder() {
    this.spinner = true;
    const measurement = {} as Measurement;

    measurement.top_total_length = this.f.top_total_length.value;
    measurement.shoulder = this.f.shoulder.value;
    measurement.chest = this.f.chest.value;
    measurement.waist = this.f.waist.value;
    measurement.hip = this.f.hip.value;
    measurement.hem = this.f.hem.value;
    measurement.sleeve_length = this.f.sleeve_length.value;
    measurement.sleeve_circ = this.f.sleeve_circ.value;
    measurement.neck_front = this.f.neck_front.value;
    measurement.neck_back = this.f.neck_back.value;

    measurement.bottom_total_length = this.f.bottom_total_length.value;
    measurement.crotch = this.f.crotch.value;
    measurement.thigh = this.f.thigh.value;
    measurement.knee = this.f.knee.value;
    measurement.ankle = this.f.ankle.value;

    const docRef = doc(this.db, CUSTOMER, this.customerUuid);
    updateDoc(docRef, { measurement })
      .then(() => {
        this.router.navigate([
          '/customer',
          this.customerUuid,
          'list-measurement',
        ]);
        this.spinner = false;
        this.presentToast('Measurements Updated');
      })
      .catch((error) => {
        this.spinner = false;
        this.presentToast(error);
      });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
