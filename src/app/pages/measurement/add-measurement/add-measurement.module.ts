import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddMeasurementPageRoutingModule } from './add-measurement-routing.module';

import { AddMeasurementPage } from './add-measurement.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    AddMeasurementPageRoutingModule,
  ],
  declarations: [AddMeasurementPage],
})
export class AddMeasurementPageModule {}
