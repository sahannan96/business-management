import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import {
  addDoc,
  collection,
  doc,
  getFirestore,
  onSnapshot,
  setDoc,
} from 'firebase/firestore';
import * as moment from 'moment';
import { ORDERS, VERDICT } from '../../../shared/constants/strings';
import { Order } from '../../../shared/interfaces/list-data-interface';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.page.html',
  styleUrls: ['./create-order.page.scss'],
})
export class CreateOrderPage implements OnInit {
  db = getFirestore();
  spinner: boolean = false;
  order: Order = {};
  customerUuid: string = '';
  dateNow = moment().format();
  orderForm = new FormGroup({
    name: new FormControl('', Validators.required),
    order_date: new FormControl(moment().format()),
    due_date: new FormControl(moment().format()),
    details: new FormControl(''),
    cost: new FormControl(null, Validators.required),
  });

  get f() {
    return this.orderForm.controls;
  }

  constructor(
    private readonly toastController: ToastController,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.spinner = true;
    this.customerUuid = this.route.snapshot.params.customerUuid;
    this.order.uuid = this.route.snapshot.params.orderUuid;
    this.order.uuid ? this.loadOrder() : (this.spinner = false);
  }

  createOrder() {
    this.spinner = true;
    const order = {} as Order;
    order.customer_uuid = this.customerUuid;
    order.name = this.f.name.value;
    order.order_date = this.f.order_date.value;
    order.due_date = this.f.due_date.value;
    order.details = encodeURI(this.f.details.value);
    order.cost = this.f.cost.value;
    order.status = VERDICT.WAITING;

    const docRef = collection(this.db, ORDERS);
    addDoc(docRef, order)
      .then((res) => {
        this.spinner = false;
        this.presentToast('Order Added');
        this.orderForm.reset();
        this.router.navigate([
          '/customer/' + this.customerUuid + '/orders/view/',
          res.id,
        ]);
      })
      .catch((error) => {
        this.spinner = false;
        this.presentToast(error);
      });
  }

  loadOrder() {
    const docRef = doc(this.db, ORDERS, this.order.uuid);
    onSnapshot(docRef, (doc) => {
      this.order = {} as Order;
      this.order = doc.data();
      this.order.uuid = doc.id;
      this.populateForm();
      this.spinner = false;
    });
  }

  populateForm() {
    this.f.name.setValue(this.order.name);
    this.f.order_date.setValue(this.order.order_date);
    this.f.due_date.setValue(this.order.due_date);
    this.f.details.setValue(decodeURI(this.order.details));
    this.f.cost.setValue(this.order.cost);
    this.spinner = false;
  }

  updateOrder() {
    this.spinner = true;
    const order = {} as Order;
    order.name = this.f.name.value;
    order.order_date = this.f.order_date.value;
    order.due_date = this.f.due_date.value;
    order.details = encodeURI(this.f.details.value);
    order.cost = this.f.cost.value;

    const docRef = doc(this.db, ORDERS, this.order.uuid);
    setDoc(docRef, order, { merge: true })
      .then(() => {
        this.spinner = false;
        this.presentToast('Order Updated');
      })
      .catch((error) => {
        this.spinner = false;
        this.presentToast(error);
      });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
