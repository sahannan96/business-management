import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import {
  deleteDoc,
  doc,
  getFirestore,
  onSnapshot,
  setDoc,
  Unsubscribe,
} from 'firebase/firestore';
import { ORDERS, VERDICT } from '../../../shared/constants/strings';
import { Order } from '../../../shared/interfaces/list-data-interface';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.page.html',
  styleUrls: ['./view-order.page.scss'],
})
export class ViewOrderPage implements OnInit {
  db = getFirestore();
  spinner: boolean = true;
  order: Order = {};
  unsubscribe: Unsubscribe;
  customerUuid: string = '';
  showUpdateButtons: boolean;
  verdict = VERDICT;
  statusColor: string;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly alertController: AlertController,
    private readonly toastController: ToastController,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.order.uuid = this.route.snapshot.params.orderUuid;
    this.customerUuid = this.route.snapshot.params.customerUuid;
    this.loadOrder();
  }

  loadOrder() {
    const docRef = doc(this.db, ORDERS, this.order.uuid);
    this.unsubscribe = onSnapshot(docRef, (doc) => {
      this.order = {} as Order;
      this.order = doc.data();
      this.order.details = decodeURI(this.order.details);
      this.order.uuid = doc.id;
      if (this.order.status === VERDICT.COMPLETED) {
        this.statusColor = 'primary';
        this.showUpdateButtons = false;
      } else if (this.order.status === VERDICT.IN_PROGRESS) {
        this.showUpdateButtons = true;
        this.statusColor = 'success';
      } else if (this.order.status === VERDICT.WAITING) {
        this.showUpdateButtons = true;
        this.statusColor = 'default';
      } else {
        this.statusColor = 'light';
        this.showUpdateButtons = false;
      }
      this.spinner = false;
    });
  }

  confirmDeleteOrder() {
    this.presentAlert(
      `Are you sure you want to delete order ${this.order.name}?`
    );
  }

  async presentAlert(msg: string, action?: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      cssClass: 'custom-alert',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'alert-button-cancel',
        },
        {
          text: 'Confirm',
          role: 'confirm',
          cssClass: 'alert-button-confirm',
          handler: () => {
            this.spinner = true;
            action ? this.updateStatus(action) : this.deleteOrder();
          },
        },
      ],
    });
    await alert.present();
  }

  async deleteOrder() {
    this.unsubscribe();
    const docRef = doc(this.db, ORDERS, this.order.uuid);
    await deleteDoc(docRef)
      .then(() => {
        this.router.navigate(['/customer', this.customerUuid, 'orders']);
        this.spinner = false;
        this.presentToast('Customer Deleted');
      })
      .catch((err) => {
        this.spinner = false;
        this.presentToast(err);
      });
  }

  updateStatus(status: string) {
    this.spinner = true;
    const docRef = doc(this.db, ORDERS, this.order.uuid);
    setDoc(docRef, { status }, { merge: true })
      .then(() => {
        this.spinner = false;
        status !== VERDICT.IN_PROGRESS || VERDICT.WAITING
          ? (this.showUpdateButtons = false)
          : (this.showUpdateButtons = true);
        this.presentToast('Status Updated');
      })
      .catch((error) => {
        this.spinner = false;
        this.presentToast(error);
      });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
