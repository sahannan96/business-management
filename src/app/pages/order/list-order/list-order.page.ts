import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  getFirestore,
  collection,
  query,
  orderBy,
  onSnapshot,
  where,
} from 'firebase/firestore';
import { ORDERS } from '../../../shared/constants/strings';
import { Order } from '../../../shared/interfaces/list-data-interface';

@Component({
  selector: 'app-list-order',
  templateUrl: './list-order.page.html',
  styleUrls: ['./list-order.page.scss'],
})
export class ListOrderPage implements OnInit {
  db = getFirestore();
  spinner: boolean = true;
  customerUuid: string = '';
  orders: Order[];
  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit() {
    this.customerUuid = this.route.snapshot.params.customerUuid;
    this.loadOrders();
  }

  loadOrders() {
    const docRef = collection(this.db, ORDERS);
    const order = query(
      docRef,
      where('customer_uuid', '==', this.customerUuid),
      orderBy('order_date', 'desc')
    );
    onSnapshot(order, (snapshot) => {
      this.orders = [];
      snapshot.docs.forEach((doc) => {
        let order = {} as Order;
        order = doc.data();
        order.uuid = doc.id;
        this.orders.push(order);
      });
      this.spinner = false;
    });
  }
}
