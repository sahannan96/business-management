import { Component, OnInit } from '@angular/core';
import {
  getFirestore,
  collection,
  query,
  onSnapshot,
  doc,
} from 'firebase/firestore';
import { CUSTOMER, ORDERS, VERDICT } from '../../../shared/constants/strings';

@Component({
  selector: 'app-list-all-orders',
  templateUrl: './list-all-orders.page.html',
  styleUrls: ['./list-all-orders.page.scss'],
})
export class ListAllOrdersPage implements OnInit {
  db = getFirestore();
  spinner: boolean = true;
  orders: any[];
  verdict = VERDICT;
  constructor() {}

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    onSnapshot(query(collection(this.db, ORDERS)), (ordersPayload) => {
      this.orders = [];
      ordersPayload.docs.forEach((orderPayload) => {
        let order = {} as any;
        order = orderPayload.data();
        order.uuid = orderPayload.id;
        onSnapshot(
          doc(this.db, CUSTOMER, order.customer_uuid),
          (customerPayload) => {
            order.customer = customerPayload.data();
            order.customer.uuid = customerPayload.id;
          }
        );
        this.orders.push(order);
      });
      this.spinner = false;
    });
  }

  getStatusColor(status: string) {
    if (status === VERDICT.COMPLETED) {
      return { '--color': 'var(--ion-color-primary)' };
    } else if (status === VERDICT.IN_PROGRESS) {
      return { '--color': 'var(--ion-color-success)' };
    } else if (status === VERDICT.WAITING) {
      return { '--color': 'var(--ion-color-default)' };
    } else {
      return { '--color': 'var(--ion-color-light)' };
    }
  }
}
