import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListAllOrdersPageRoutingModule } from './list-all-orders-routing.module';

import { ListAllOrdersPage } from './list-all-orders.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListAllOrdersPageRoutingModule
  ],
  declarations: [ListAllOrdersPage]
})
export class ListAllOrdersPageModule {}
