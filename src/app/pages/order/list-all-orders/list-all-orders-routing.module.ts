import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListAllOrdersPage } from './list-all-orders.page';

const routes: Routes = [
  {
    path: '',
    component: ListAllOrdersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListAllOrdersPageRoutingModule {}
