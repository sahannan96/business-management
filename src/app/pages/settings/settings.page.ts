import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { getAuth, onAuthStateChanged, signOut } from 'firebase/auth';
import { doc, getFirestore, onSnapshot, setDoc } from 'firebase/firestore';
import { DARK_MODE, OFF, ON, USERS } from '../../shared/constants/strings';
import { StorageService } from '../../shared/services/storage.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  db = getFirestore();
  theme: string;
  brand: string = '';
  uuid: string;
  spinner: boolean = true;
  constructor(
    private readonly storage: StorageService,
    private readonly router: Router,
    private readonly toastController: ToastController
  ) {}

  ngOnInit() {
    this.storage.getItem('user').then((user) => {
      this.uuid = user;
      this.checkTheme();
      this.loadBrand();
    });
    this.spinner = false;
  }

  checkTheme() {
    this.storage.getItem(DARK_MODE).then((darkMode) => {
      if (darkMode === ON) {
        this.theme = 'dark';
      } else {
        this.theme = 'light';
      }
    });
  }

  themeChanged(event) {
    if (event.detail.value === 'dark') {
      this.storage.setItem(DARK_MODE, ON);
      document.body.setAttribute('color-theme', 'dark');
    } else {
      this.storage.setItem(DARK_MODE, OFF);
      document.body.setAttribute('color-theme', 'light');
    }
  }

  signout() {
    const auth = getAuth();
    signOut(auth)
      .then(() => {
        this.storage.db.clear();
        this.presentToast('Successfully logged out');
        this.router.navigate(['/login']);
      })
      .catch((error) => {
        this.presentToast(error);
      });
  }

  loadBrand() {
    const docRef = doc(this.db, USERS, this.uuid);
    onSnapshot(docRef, (doc) => {
      this.brand = doc.get('brand');
    });
  }

  updateBrand() {
    this.spinner = true;
    const docRef = doc(this.db, USERS, this.uuid);
    setDoc(docRef, { brand: this.brand }, { merge: true })
      .then(() => {
        this.spinner = false;
        this.presentToast('Brand Updated');
      })
      .catch((error) => {
        this.spinner = false;
        this.presentToast(error);
      });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
