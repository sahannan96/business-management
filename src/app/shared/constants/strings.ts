export const CUSTOMER = 'customer';
export const ORDERS = 'orders';
export const USERS = 'users';
export const DARK_MODE = 'dark_mode';
export const DARK_MODE_CLASS_NAME = 'darkMode';
export const ON = 'on';
export const OFF = 'off';

export const VERDICT = {
  COMPLETED: 'Completed',
  IN_PROGRESS: 'In Progress',
  CANCELED: 'Canceled',
  WAITING: 'Waiting',
};
