import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { share } from 'rxjs/operators';
import { Storage } from '@capacitor/storage';

export const APP_INIT = 'app_init';
export const GET_ITEM = 'get_item';
export const SET_ITEM = 'set_item';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  public db = Storage;
  private onSubject = new BehaviorSubject<{ event: string; value: any }>({
    event: APP_INIT,
    value: true,
  });
  public changes = this.onSubject.asObservable().pipe(share());

  async getItem(key: string) {
    const value = await this.db.get({ key });
    this.onSubject.next({ event: GET_ITEM, value });
    return value?.value;
  }

  async setItem(key: string, value: string) {
    this.onSubject.next({ event: SET_ITEM, value: { key, value } });
    return await this.db.set({ key, value });
  }
}
