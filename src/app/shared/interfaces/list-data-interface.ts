export interface User {
  uuid?: string;
  email?: string;
  name?: string;
  photo?: string;
  phone?: string;
}

export interface Customer {
  uuid?: string;
  name?: string;
  phone?: number;
  email?: string;
  address?: string;
  measurement?: Measurement;
}

export interface Order {
  uuid?: string;
  customer_uuid?: string;
  name?: string;
  order_date?: any;
  due_date?: any;
  details?: string;
  cost?: number;
  status?: string;
}

export interface Measurement {
  top_total_length?: number;
  shoulder?: number;
  chest?: number;
  waist?: number;
  hip?: number;
  hem?: number;
  sleeve_length?: number;
  sleeve_circ?: number;
  neck_front?: number;
  neck_back?: number;

  bottom_total_length?: number;
  crotch?: number;
  thigh?: number;
  knee?: number;
  ankle?: number;
}
