import { Component } from '@angular/core';
import { initializeApp } from 'firebase/app';
import { environment } from '../environments/environment';
import { DARK_MODE, ON } from './shared/constants/strings';
import { StorageService } from './shared/services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(private readonly storage: StorageService) {}
  app = initializeApp(environment.firebaseConfig);

  ngOnInit() {
    this.setTheme();
  }

  setTheme() {
    this.storage.getItem(DARK_MODE).then((darkMode) => {
      if (darkMode === ON) {
        document.body.setAttribute('color-theme', 'dark');
      } else {
        document.body.setAttribute('color-theme', 'light');
      }
    });
  }
}
