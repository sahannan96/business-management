import { Component, OnInit } from '@angular/core';
import { PopoverController, ToastController } from '@ionic/angular';
import { doc, getFirestore, setDoc } from 'firebase/firestore';
import { USERS } from '../../shared/constants/strings';
import { StorageService } from '../../shared/services/storage.service';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss'],
})
export class BrandComponent implements OnInit {
  db = getFirestore();
  brand: string = '';
  constructor(
    private readonly storage: StorageService,
    private readonly toastController: ToastController,
    private readonly popoverController: PopoverController
  ) {}

  ngOnInit() {}

  updateBrand() {
    this.storage.getItem('user').then((user) => {
      const docRef = doc(this.db, USERS, user);
      setDoc(docRef, { brand: this.brand }, { merge: true })
        .then(() => {
          this.presentToast('Setup complete');
          this.popoverController.dismiss();
        })
        .catch((error) => {
          this.presentToast(error);
        });
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
