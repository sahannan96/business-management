import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import {
  collection,
  doc,
  getDoc,
  getFirestore,
  onSnapshot,
  query,
} from 'firebase/firestore';
import { CUSTOMER, USERS } from '../shared/constants/strings';
import { User } from '../shared/interfaces/list-data-interface';
import { BrandComponent } from './brand/brand.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  db = getFirestore();
  sales: number = 737;
  customers: number = 425;
  products: number = 1234;
  revenue: number = 4325;
  brand: string = '';
  auth = getAuth();
  user: User = {};
  spinner: boolean = true;
  constructor(
    private readonly router: Router,
    private readonly popoverController: PopoverController
  ) {}

  ngOnInit() {
    this.checkLogin();
  }

  checkLogin() {
    onAuthStateChanged(this.auth, (user) => {
      if (user) {
        this.user.uuid = user.uid;
        this.user.name = user.displayName;
        this.user.phone = user.phoneNumber;
        this.user.photo = user.photoURL;
        this.user.email = user.email;
        this.loadBrand();
      } else {
        this.router.navigate(['/login']);
      }
    });
  }

  loadBrand() {
    const docRef = doc(this.db, USERS, this.user?.uuid);
    getDoc(docRef).then((doc) => {
      this.brand = doc.get('brand');
      if (!this.brand) {
        this.presentPopover();
      } else {
        this.spinner = false;
      }
    });
  }

  async presentPopover() {
    const popover = await this.popoverController.create({
      component: BrandComponent,
    });

    await popover.present();
    await popover.onDidDismiss().then(() => {
      this.loadBrand();
    });
  }

  getCutomersCount() {
    const docRef = collection(this.db, CUSTOMER);
    const order = query(docRef);
    onSnapshot(order, (snapshot) => {
      this.customers = snapshot.docs.length;
    });
  }
}
